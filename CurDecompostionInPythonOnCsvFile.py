import numpy as np
from typing import Tuple
from numpy import linalg as LA

Matrix = np.ndarray
Vector = np.ndarray

def cur_decomposition(M: Matrix, r: int) -> Tuple[Matrix, Matrix, Matrix]:
    """CUR decomposition
    Args:
        M: np.ndarray [M, N], matrix to decompose
        r: int, rank
    Return:
        C: Column Matrix: [M, r] (sparse)
        U: U Matrix: [r, r] (dense)
        R: Row Matrix: [r, N] (sparse)
    """
    m, n = M.shape
    r_probs, c_probs = probabilities(M)
    C, c_idx = select_C(M, r, c_probs)
    R, r_idx = select_R(M, r, r_probs)
    U = make_U(M, c_idx, r_idx)
    return C, U, R

def probabilities(M: Matrix) -> Tuple[Vector, Vector]:
    """Define probabilities to sample rows and columns by,"""
    squared = np.square(M)
    row_sum = np.sum(squared, 1)
    col_sum = np.sum(squared, 0)
    denom = np.sum(row_sum)
    row_probs = row_sum / denom
    col_probs = col_sum / denom
    return row_probs, col_probs

def select_C(M, r, probs):
    return select_part(M, r, probs, 1)

def select_R(M, r, probs):
    return select_part(M, r, probs, 0)

def select_part(M: Matrix, r: int, probs: Vector, axis: int) -> Tuple[Vector, Vector]:
    size = M.shape[axis]
    probs1 = probs
    # gets first r minimum prob 
    scale = getMin(probs1,r,axis)
    scale = np.array(scale)
    # retrive indexes of first r minimum prob
    idx = getIndex(probs, scale)
    idx = np.array(idx)
    #Selected the following idx data to construct C and R
    selected = np.take(M, idx, axis) 
    scale = np.sqrt(scale * r)
    scale = np.expand_dims(scale, axis - 1)
    # return selected matrix i.e (C or R) and following indexes which are choosen
    return selected / scale, idx

def getIndex(probs,scale):
    idx =[]
    cnt =0
    for x in scale:
        for i in range(len(probs)):
            if cnt == 0:
                cnt =1
                if (x == probs[i]):
                    idx.append(i)
                    break
            else : 
                if (i not in idx) == True:
                    if (x == probs[i] ):
                        idx.append(i)
                        break
    return idx
        
def getMin( probs,  r,axis):
    if (axis == 0):
        probs = np.sort(probs)
        probs =  [i for i in probs if i != 0]
        return probs[0:r]
    else:
        probs = np.sort(probs)
        probs =  [i for i in probs if i != 0]
        return probs[0:r]
    
def make_U(M: Matrix, c: Vector, r: Vector) -> Matrix:
    W = select_W(M, c, r)
    x, e, y = np.linalg.svd(W)
    inv_e = psuedo_inverse(e)
    U = y.T @ np.diag(np.square(inv_e)) @ x.T
    return U

def select_W(M: Matrix, c: Vector, r: Vector) -> Vector:
    return M[r, :][:, c]

def psuedo_inverse(sigma: Vector) -> Vector:
    zeros = sigma == 0
    num = (zeros) == 0
    denum = zeros + sigma
    return num / denum

mat  = np.loadtxt(open("/csv/file/directory", "rb"), delimiter=",", skiprows=0)
A = mat[:,1:]
C, U, R = cur_decomposition(A, 10)
im = np.dot(np.dot(C,U), R)

print("Error 	" + LA.norm(A-im))