import psycopg2
import re
import numpy as np
from numpy import linalg as LA
from sklearn.metrics import mean_squared_error
import time
def connect(x):
	try:
		connection = psycopg2.connect(user="postgres", password="q",host="192.168.43.194",port="30001",database=x)
		return connection
	except (Exception, psycopg2.Error) as error :
		print("Unable to connect", error)

def nodes(table):
	query = """ select xc_node_id from """+table+""" group by xc_node_id;"""
	cursor.execute(query)
	nodes = cursor.fetchall()
	n =[]
	for node in nodes:
		n.append(node[0])
	return n
	
def columns(table):
	query =  """ SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"""+table+"""' order by ordinal_position;"""
	cursor.execute(query)
	column = cursor.fetchall()
	col =[]
	for x in column:
		col.append(x[0])
	return col

def table_sum(table,node,col):
	column=""
	cnt =0
	for x in col:
		if cnt==0:
			cnt=1
		else:
			column =  column + """power( """ + str(x) + """,2) +"""
	column = column[:-1]
	y=""
	for x in node:
		y = y +""" sum (""" +column + """ ) filter (where xc_node_id="""+ str(x)+"""),"""
	y = y[:-1]
	query = """select """ + y + """ from """ + table + """ ;"""
	cursor.execute(query) 
	x = cursor.fetchall()
	sums =[]
	for xi in  range(0,len(node)):
		sums.append(x[0][xi])
	return sums 

def ProbabilityOfRow_name(table,sums,node,cols,ids):
	a = ""
	cnt=0
	for x in cols:
		if cnt==0:
			cnt=1
		else:
			a = a + """POWER("""+ x+""",2)+"""
	a=a[:-1]
	a = """ ( """+a+""" ) """
	st="""CASE """
	if len(node)==1:
		st =st +""" when xc_node_id = """+str(node[0])+""" then """+a+""" /"""+str(sums[0])+""" END """
	else:
		for x in range(0,len(node)-1):
			st = st+""" when xc_node_id = """+str(node[x])+""" then """+a+""" /"""+str(sums[x])
			#print(str(sums[x]))
		st = st+""" else """ +a+"""/"""+str(sums[len(sums)-1])+""" END """
	query = """Create table selectedrandom_row (row_id , probability)  distributed by (row_id)  """ +ids[-1]+""" AS select row_id, """+st+""" from """+ table+"""  ;"""
	cursor.execute(query)
	connection.commit()
	query = """alter table selectedrandom_row add column flag integer default 0;"""
	cursor.execute(query)
	connection.commit()
	#print(query)

def update_intermediate_table(r, num_of_nodes):
	for i in range(num_of_nodes):
    		query = '''execute direct on (dn''' + str(i+1) + ''') 'update selectedrandom_row set flag = 1 where row_id IN (select row_id from selectedrandom_row order by probability desc limit ''' + str(r) +''')';'''
    		cursor.execute(query)
    		connection.commit()


def sum_columnsquare(cnt,table_name, names, n, q,sums):
	query = ''' select '''
	for i in n:
		query = query + '''((coalesce(sum(POWER(''' + names + ''',2)) filter (where xc_node_id='''+str(i)+'''))))'''+''','''
	query = query[:-1]
	query = query + """ from """ + table_name
	cursor.execute(query)
	# print(query)
	connection.commit()
	prob = cursor.fetchall()
	#print(prob[0][0])
	q=""
	for i in range(len(n)):
		q = q + '''(''' +str(cnt*len(n)+i)+""",""" +str(n[i]) + """, """ + str(prob[0][i]/sums[i]) + """ , '""" + names + """', 0),"""
	#print(q)
	return q


def probabilityOfColumn(table_name, n,node,ids,sums):
	query = '''CREATE TABLE selectedrandom_column(id int,node_id bigint, probability double precision,column_name character varying,flag integer DEFAULT 0) '''+ ids[-1]+''' ;'''
	cursor.execute(query)
	connection.commit() 	
	query = ''' insert into selectedrandom_column values '''
	cnt=0
	for i in n:
		if cnt==0:
			cnt=1
		else:
			query = query + sum_columnsquare(cnt,table_name, i, node, query,sums)
		cnt=cnt+1
	query = query[:-1]+";"
	cursor.execute(query)
	connection.commit()

def update_intermediate_table_column(node, r,ids):
	x=0
	while x!=r:
		query ="""  select probability,xc_node_id,column_name,id from selectedrandom_column where probability in (select  max(probability) from selectedrandom_column where flag=0  and column_name not in (select  column_name from selectedrandom_column where flag=1 ) limit 1) and flag=0  limit 1;"""
		cursor.execute(query)
		v  = cursor.fetchall()
		for i in range(0,len(node)):
			#print(str(node[i]))
			if v[0][1]==node[i]:
				x = ids[i]
				break
		#print(str(v[0][2]))
		query = '''execute direct on ('''+str(x)+""") 'update  selectedrandom_column set flag = 1 where id="""+str(v[0][3])+""" ';"""
		#probability = (select probability from selectedrandom_column where probability =""" + str(v[0][0]) +""" and flag=0 limit 1)  and flag=0 and  node_id="""+ str(v[0][3])+"""  ;'"""
		cursor.execute(query)
		connection.commit()
		query ="""select count(distinct column_name) from selectedrandom_column where flag=1; """
		cursor.execute(query)
		x =cursor.fetchall()
		x=x[0][0]
		
def update_row_table(col,rank):
	query = """update row_table set """
	cnt =0
	for i in col:
		if cnt ==0:
			cnt=1
		else:
			query = query + str(i) + """= """ + str(i)  +"""/sqrt(selectedrandom_row.probability *"""+str(rank) +"""), """
	query = query[:-2] + """ from selectedrandom_row where selectedrandom_row.row_id=row_table.row_id; """
	#print(query)
	cursor.execute(query)
	connection.commit()

def update_column_table(rank,node,sums):
	query ="""select column_name from selectedrandom_column where flag=1 """
	cursor.execute(query)
	column = cursor.fetchall()
	col =[]
	for x in column:
		col.append(x[0])
	for x in range(0,len(node)):
		for i in col:
			query = """update column_table set """+str(i) + """ = """+str(i)+"""/sqrt( ((select probability from selectedrandom_column where node_id =""" + str(node[x])+""" and column_name like '"""+ str(i)+"""')*""" +str(rank)+"""));"""
			cursor.execute(query)
			connection.commit()	

def rf(table,rank,node,col,sums,ids):
	ProbabilityOfRow_name(table,sums,node,col,ids)
	update_intermediate_table(rank,len(node))
	query = """create table row_table """+ids[-1] +""" AS SELECT  *  FROM """+table +"""  where row_id in(select row_id from selectedrandom_row where flag=1) """;
	cursor.execute(query)
	connection.commit()
	update_row_table(col,rank)
	probabilityOfColumn(table,col,node,ids,sums)
	update_intermediate_table_column(node,rank,ids)
	query = """ select string_agg(column_name, ',') from (select distinct column_name from selectedrandom_column where flag=1 ) AS derivedTable;"""
	cursor.execute(query)
	p = cursor.fetchall()
	p =str(p)
	p=p[3:-4]
	#print(p)
	query = """ create table column_table """+ ids[-1]+""" as select row_id,"""+ p + """ from """+ table+""" ; """
	cursor.execute(query)
	connection.commit()
	update_column_table(rank,node,sums)
	query = """ create table u_table """+ids[-1] +""" as select row_id,""" + p + """ from  """+table + """ where row_id in (select row_id from row_table);"""
	cursor.execute(query)
	connection.commit()
	find_u('u_table',node,rank,ids)

def apply_svd(m,datanode,rank):
	x, s, y = np.linalg.svd(m, full_matrices=True)
	s=np.diag(s)
	r,c=s.shape
	#print(r,c)
	ps=np.zeros((r,c))
	for i in range(r):
	    for j in range(c):
	    	if s[i][j] != 0 :
	    		ps[i][j]=1/s[i][j]
	#print(m)
	psq=np.square(ps)
	xt=x.transpose()
	u1=np.matmul(y,psq)
	u=np.matmul(u1,xt)
	a=np.squeeze(np.asarray(u))
	b,c=a.shape
		
		
	q=""
	for j in range(c):
	    q += "a"+str(j)+" float,"
	q=q[:-1]

	q1="create table if not exists u1_table( "+q+" )"
	#print(q1)
	cursor.execute(q1)
	connection.commit()
	q="execute direct on ("+datanode+") 'insert into u1_table values("
	for j in range(b):
		for i in range(c):
			q=q+str(a[j][i])+","
		q=q[:-1]
		q+="),("
	q=q[:-2]
	q=q+"';"
	#print(q)
	cursor.execute(q)
	connection.commit()

def find_u(table_name, nodes,r,ids):
	query = "select *,xc_node_id from "+ table_name + " order by xc_node_id;"
	cursor.execute(query)
	rv = cursor.fetchall()
	#print(len(rv[0]))
	m=np.array(rv)
	#print(m[0][0])
	for k in range(0,len(nodes)):
		if k==0:
			mat=m[0:r,1:-1]
		else:
			mat =m[(k-1)*r:k*r,1:-1]
		#print(mat.shape)
		quer = " select node_name from pgxc_node where node_id=" + str(m[k*r][-1])+";"
		cursor.execute(quer)
		res=cursor.fetchall()
		#print(res[0][0])
		apply_svd(mat,res[0][0],r)
			
def reconst_error(table,col,node):
	error =[]
	e_sum=0
	for f in node:
		query =""" Select * from """+table+ """ where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from """+table +""" where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(len(col)):
			s=[]
			if i!=0:
				for k in range(n_rows):
					s.append(data[k][i])
				l.append(s)
		A = np.matrix(l)
		A = A.transpose()

		m = columns('column_table')
		query =""" Select * from column_table where xc_node_id = """+ str(f)+ """;"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from column_table where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(1,len(m)):
			s=[]
			for k in range(n_rows):
				s.append(data[k][i])
			l.append(s)
		C = np.matrix(l)
		C =C.transpose()
		
		m = columns('u1_table')
		query =""" Select * from u1_table where xc_node_id = """+ str(f)+ """;"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from u1_table where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(0,len(m)):
			s=[]
			for k in range(n_rows):
				s.append(data[k][i])
			l.append(s)
		U = np.matrix(l)
		U =U.transpose()

		m = columns('row_table')
		query =""" Select * from row_table where xc_node_id = """+ str(f)+ """;"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from row_table where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(1,len(m)):
			s=[]
			for k in range(0,n_rows):
				s.append(data[k][i])
			l.append(s)
		R = np.matrix(l)
		R =R.transpose()

		im = np.matmul(C,U)
		im = np.matmul(im,R)
		print(LA.norm(A))
		print(LA.norm(im))
		e_s = LA.norm(A-im)
		error.append(e_s)
		e_sum = e_sum+e_s
	print(error)
	return e_sum

def cur_table(table,col,node,ids):
	query = """ create table cur ( standard_relation float, published_units float, standard_units float, standard_flag float, standard_type float, published_type float, data_validity_comment float, potential_duplicate float, published_relation float, pchembl_value float, type float, pref_name float, max_phase float, therapeutic_flag float, dosed_ingredient float, structure_type float, chebi_par_id float, molecule_type float, oral float, parenteral float, topical float, black_box_warning float, natural_product float, first_in_class float, chirality float, prodrug float, inorganic_flag float, availability_type float, polymer_flag float, standard_inchi float, standard_inchi_key float, canonical_smiles float, assay_test_type float, assay_category float, assay_organism float, assay_strain float, assay_tissue float, assay_cell_type float, relationship_type float, confidence_score float, target_type float, organism float, species_group_flag float)  """+ids[-1]
	cursor.execute(query)
	connection.commit()
	cnt=0
	for f in node:
		query1 = """execute direct on ("""+ids[cnt]+""") ' insert into cur   values """
		cnt=cnt+1
		m = columns('column_table')
		query =""" Select * from column_table where xc_node_id = """+ str(f)+ """;"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from column_table where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(1,len(m)):
			s=[]
			for k in range(n_rows):
				s.append(data[k][i])
			l.append(s)
		C = np.matrix(l)
		C =C.transpose()
		
		m = columns('u_table')
		query =""" Select * from u_table where xc_node_id = """+ str(f)+ """;"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from u_table where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(1,len(m)):
			s=[]
			for k in range(n_rows):
				s.append(data[k][i])
			l.append(s)
		U = np.matrix(l)
		U =U.transpose()

		m = columns('row_table')
		query =""" Select * from row_table where xc_node_id = """+ str(f)+ """;"""
		cursor.execute(query)
		data = cursor.fetchall()
		query =""" select count(*) from row_table where xc_node_id = """+ str(f)+""";"""
		cursor.execute(query)
		n_rows = cursor.fetchall()
		n_rows = n_rows[0][0]
		l=[]
		for i in range(1,len(m)):
			s=[]
			for k in range(0,n_rows):
				s.append(data[k][i])
			l.append(s)
		R = np.matrix(l)
		R =R.transpose()
		
		im = np.matmul(C,U)
		im = np.matmul(im,R)
		ro,co = im.shape 
		
		for i in range(ro):
			x = im[i]
			x = x.tolist()
			x =str(x)
			x =x[2:-2]
			x=x.replace("  ", " , ")
			x = """ ("""+x+"""),"""
			query1 = query1 +x
		
		query1 = query1[:-1]+""" ' """
		cursor.execute(query1)
		connection.commit()


def drop_table():
	query="drop table if exists row_table,column_table,u_table,u1_table,selectedrandom_column,selectedrandom_row,cur;"
	cursor.execute(query)
	connection.commit()

def node_ids(node):
	xx=[]
	ids=" to node ("
	for i in node:
		query =""" select node_name from pgxc_node where node_id ="""+str(i)+""" ;"""
		cursor.execute(query)
		x =cursor.fetchall()
		xx.append(x[0][0])
		ids=ids+str(x[0][0])+","
	ids=ids[:-1]+" )"
	xx.append(ids)
	return xx


def cur(table,rank):
	drop_table()
	start_time = time.time()
	node =nodes(table)
	col = columns(table)
	ids=node_ids(node)
	sums = table_sum(table,node,col)
	rf(table,rank,node,col,sums,ids)
	e = reconst_error(table,col,node)
	#Time for computing CUR
	print("--- %s seconds ---" % (time.time() - start_time))
	print(table)
	print(rank)
	print("error")
	print(e)

#Database connection
connection = connect('postgres')
cursor = connection.cursor()
#table on which cur should be applie and rank r
cur('sample',3)
cursor.close()
connection.close()



'''
Consider a table with following columns - row_id, c1,c2 distributed on 2 data nodes.
Here the how the algorithm of above following code - 

Find the no. of datanodes and following node_ids for computations - select xc_node_id from sample group by xc_node_id;

Find the columns of the following table - SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'sample' order by ordinal_position;

Compute sum of values on each individual data node - select  sum (power( c1,2) +power( c2,2) +power( c3,2)  ) filter (where xc_node_id=-1268658584), sum (power( c1,2) +power( c2,2) +power( c3,2)  ) filter (where xc_node_id=-700122826) from sample ;

Function - ProbabilityOfRow_name

	Find probability of each row and insert them into a new table (selectedrandom_row with columns row_id , probability, flag - row id corresponds row_id vaue in table and follows by their probability and flag default value 0
	flag value 1 indicates it was choosen as a row in Row table(R)
	Probability of row = square(c1)+square(c2)/sum(datanode of row exists)

	 Queries -  Create table selectedrandom_row (row_id , probability)  distributed by (row_id)   to node (dn1,dn2,) AS select row_id, CASE  when xc_node_id = -1268658584 then  ( POWER(c1,2)+POWER(c2,2)+POWER(c3,2) )  /15476.0 when xc_node_id = -700122826 then  ( POWER(c1,2)+POWER(c2,2)+POWER(c3,2) )  /12665.0  END  from sample  ;
   
   and then update flag = 1 for r rows based on the probability on each datanode using update_row_table and update_intermediate_table

Similary calculate probability of each column in each datanode and store the values selectedrandom_column

Query - CREATE TABLE selectedrandom_column(id int,node_id bigint, probability double precision,column_name character varying,flag integer DEFAULT 0)  to node (dn1,dn2 ) ;

Probability of each column -  insert into selectedrandom_column values (13,352366662, 0.42001655781436853 , 'c1', 0),(14,823103418, 0.17550655542312277 , 'c1', 0),(17,-560021589, 0.3866016486706142 , 'c2', 0),(18,352366662, 0.19133474381381657 , 'c2', 0),(22,-560021589, 0.3551608034366655 , 'c3', 0),(23,352366662, 0.3886486983718149 , 'c3', 0);

And then update the flag to 1 for r distinct column in selectedrandom_column  and used the following in C table.
Query - select probability,xc_node_id,column_name,id from selectedrandom_column where probability in (select  max(probability) from selectedrandom_column where flag=0  and column_name not in (select  column_name from selectedrandom_column where flag=1 ) limit 1) and flag=0  limit 1;
	execute direct on (dn3) 'update  selectedrandom_column set flag = 1 where id=16 ';

Creation of R table - create table row_table  to node (dn1,dn2 ) AS SELECT  *  FROM sample  where row_id in(select row_id from selectedrandom_row where flag=1) 

C table - create table column_table  to node (dn5,dn3,dn1,dn2,dn4 ) as select row_id,c1,c2,c3 from sample ; ( columns selected in selectedrandom_flag)

U table - (Intersection of C and R table)
 - create table u_table  to node (dn5,dn3,dn1,dn2,dn4 ) as select row_id,c1,c2,c3 from  sample where row_id in (select row_id from row_table);

Apply SVD on U table and create u1 table 







'''