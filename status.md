# Weekly status report

---

## Week of Jun 17

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Discussed about the problem statement with the mentor.

### Issues/Concerns/Road blocks
  1. No issues as of now.


### Plans for the next week
  1. Implement a python code and know how use matrix decomposition functions.
  2. Compare all the algorithms and analyse all the algorithms, there time complexity.

### Comments from guide (very specific)
  - Know pros and cons of the project.
  
---

## Week of Jun 24

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Gone through python libraries and learned about matrix decomposition functions and their parameters.
  2. Also analyzed the complexities and various other properties.



### Plans for the next week
  1. Go through papers and materials to get to know about the algorithms of the matrix decomposition functions.

### Comments from guide (very specific)
  1. Learn about the decompositions algorithms in depth. 


---

## Week of Jul 1

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Gone throught papers on distrubuted databases.

---

## Week of Jul 8, Jul 15,22,29

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1.  Couldn't work much due to the placements.

---

## Week of Aug 5

### Phase
  - [ ] Team formation
  - [*] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Searched for datasets and the domain which needs dimensionality reduction.
  2. Searched for papers on Dimensionality reduction and Distributed databases.

### Issues/Concerns/Road blocks
  1. Searching for datasets became complicated as we have to analyse the dataset set and its domain and decide whether it requires dimensionality reduction.
  2. The datset must also be homogeneous and distributed in multiple databases.

### Action plan to address issues
  1. Have to analyse the choosen dataset and look for the tables that gets effected after Dimensionality reduction.

### Plans for the next week
  1. Finalize the dataset and learn about it.

### Comments from guide (very specific)
  1. Research more about the dataset. 
---

## Week of Aug 12

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Selected ChEMBL as the dataset.
  2. Gone through many articles and talks on ChEMBL database to know the significance of it, so as to increase our domain knowledge.

### Issues/Concerns/Road blocks
  1. Need to find out the target label.

### Action plan to address issues
  1. Learn more about the entities of ChEMBL.

### Plans for the next week
  1. Find out which entities are required to the target feature.

### Comments from guide (very specific)
  1. Figure out what is the target label in our dataset.
---

## Week of Aug 19

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Found out the target feature and entities it dependent upon.
  2. Gone throught sql code related to CUR decomposition.

### Plans for the next week
  1. Apply the CUR decomposition algorithm on a sample databse.

### Comments from guide (very specific)
  1. Understand and apply SQL code of CUR decomposition on a sample dataset.
---

## Week of Aug 26

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Applied SQL code on a sample database.
  2. Downloaded Chembl mysql dump and loaded into our local mysql database.

### Issues/Concerns/Road blocks
  1. As the dataset is huge, took lot of time for loading the data into MySQL database.

### Plans for the next week
  1. Read papers on Distributed databases.

### Comments from guide (very specific)
  1. Select a platform for distributed RDBMS. 

---

## Week of Sep 2

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Gone through papers based on Distributed databases. 
  2. Concluded that Postgres-XL outstands all the other distributed platforms.

### Plans for the next week
  1. Learn about Postgres-XL for distrubuted computing.

### Comments from guide (very specific)
  1. Go through Postgres-XL documentation.
---

## Week of Sep 9

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [X] Report writing

### Accomplishments of the week
  1. Installed and configured Postgres-XL.
  2. Did presentation for review.

### Issues/Concerns/Road blocks
  1. Faced many error while configuring Postgres-XL.

### Action plan to address issues
  1. Need to go through Postgres-XL tutuorial once again and fix the bugs.

### Plans for the next week
  1. Complete the configuration of postgres-xl and do distrubuted computing.

### Comments from guide (very specific)
  1. Make changes in the presentation for review.
---

## Week of Sep 16

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Didn't do much beacuse of periodicals.


## Week of Sep 23

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [X] Report writing

### Accomplishments of the week
  1. Made changes in the presentation.
  2. Got to know the architecture of the Postgres-XL.
  3. Wrote a python code to merge the required tables and extracted the data into CSV for further preprocessing.

### Issues/Concerns/Road blocks
  1. Couldn't join the tables through MySQL workbench since the dataset is very large. 

### Action plan to address issues
  1. Wrote python code to merge the tables.

### Plans for the next week
  1. Start preprocessing the data. Figure out the best way to represent strings or formulas in the form of numericals.



---

## Week of Sep 30


### Accomplishments of the week
  1. Couldn't do much due to vacation.

---

## Week of Oct 7

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Continued solving the issues faced while configuring Postgre-XL. 



### Action plan to address issues
  1. Talk to mentor about the isses faced.

### Plans for the next week
  1. Complete the configuration and try to retrieve information.


### Comments from guide (very specific)
  1. Check the versions and try reinstalling the related software.

